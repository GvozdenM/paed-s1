const augmentation = require('../src/PlayerObjectAugmenter');
/**
 * @type {Player}
 */
const sampleInputPlayer = {
    name: "Stardust",
    total_kills: 298,
    total_deaths: 189,
    total_assistances: 120,
    nationality: "Serbian",
    main_position: "ADC",
    main_champions: [
        {name: "Vayne"}
    ]
};

const sampleInputPlayerKda = 1.894;

const mockTeamWinrate = 56.6;

describe('player augmentation function', () => {
    const augmentedPlayer = augmentation.augmentTeamPlayerObjects(mockTeamWinrate, sampleInputPlayer);

    it('adds method getKda', () => {
        expect(augmentedPlayer.getKda).toBeDefined();
    });

    it('added method getKda returns number', () => {
        expect(typeof augmentedPlayer.getKda).toBe("number");
    });

    it('added method getKda returns ~1.894 player with 298 kills 120 assists and 189 deaths', () => {
        expect(augmentedPlayer.getKda).toBeCloseTo(sampleInputPlayerKda);
    });

    it('added method getKda returns 1 for player with 1 kill 2 assists and 2 deaths', () => {
        const newSamplePlayer = {
            name: "Milkbreeze",
            total_kills: 1,
            total_deaths: 2,
            total_assistances: 2,
            nationality: "Maltese",
            main_position: "Mid",
            main_champions: [
                {name: "Corki"}
            ]
        };

        const newSamplePlayerKda = 1.00;

        const newAugmentedPlayer = augmentation.augmentTeamPlayerObjects(mockTeamWinrate, newSamplePlayer);

        expect(newAugmentedPlayer.getKda).toBeCloseTo(newSamplePlayerKda);
    });

    it('added method getKda does not throw and returns kills + (assists / 2) when player has 0 deaths', () =>{
        const newSamplePlayer = {
            name: "oG_98",
            total_kills: 1,
            total_deaths: 0,
            total_assistances: 2,
            nationality: "French",
            main_position: "Top",
            main_champions: [
                {name: "Ryze"}
            ]
        };

        const newSamplePlayerKda = 2.00;

        const newAugmentedPlayer = augmentation.augmentTeamPlayerObjects(mockTeamWinrate, newSamplePlayer);

        expect(newAugmentedPlayer.getKda).toBeCloseTo(2.00);
    });

    it('adds property winrate that corresponds to the value of the team winrate passed', () => {
        expect(augmentedPlayer.winrate).toBeDefined();
        expect(augmentedPlayer.winrate).toEqual(mockTeamWinrate);
    });

    it('makes player name property private and exposes getter', () => {
        expect(augmentedPlayer.name).toBeDefined();
        expect(augmentedPlayer.name).toEqual(sampleInputPlayer.name)
    });

    it('maintains player total kills', () => {
        expect(augmentedPlayer.total_kills).toBeDefined();
        expect(augmentedPlayer.total_kills).toEqual(sampleInputPlayer.total_kills);
    });

    it('maintains player death count', () => {
        expect(augmentedPlayer.total_deaths).toBeDefined();
        expect(augmentedPlayer.total_deaths).toEqual(sampleInputPlayer.total_deaths);
    });


    it('maintains player assist count', () => {
        expect(augmentedPlayer.total_assistances).toBeDefined();
        expect(augmentedPlayer.total_assistances).toEqual(sampleInputPlayer.total_assistances);
    });

    it('maintains player nationality', () => {
        expect(augmentedPlayer.nationality).toBeDefined();
        expect(augmentedPlayer.nationality).toEqual(sampleInputPlayer.nationality);
    });

    it('maintains player main position', () => {
        expect(augmentedPlayer.main_position).toBeDefined();
        expect(augmentedPlayer.main_position).toEqual(sampleInputPlayer.main_position);
    });

    it('maintains player main position', () => {
        expect(augmentedPlayer.main_champions).toBeDefined();
        expect(augmentedPlayer.main_champions.length).toEqual(sampleInputPlayer.main_champions.length);

        for(let i = 0; i < augmentedPlayer.main_champions.length; i++) {
            expect(augmentedPlayer.main_champions[i].name).toEqual(sampleInputPlayer.main_champions[i].name);
        }
    });
});