const Comparator = require('../../src/utils/Comparator');
const fs = require('fs');
const comparator = new Comparator();


/**
 * @param assertion {Function} assertions (expects)
 * @param testCase {*} data to test
 * @param validCase {*} expected output on success
 * @param testTarget {Function} target function to test
 */
function syncRun (assertion, validCase, testCase, testTarget) {assertion(testTarget(testCase, comparator), validCase)}

/**
 * @param assertion {Function} assertions (expects)
 * @param testCase {*} data to test
 * @param validCase {*} expected output on success
 * @param testTarget {Function} target function to test
 */
function asyncRun(assertion, testCase, validCase, testTarget) {
    testTarget(testCase, comparator)
        .then((result) => {
            // console.log(validCase, result);
            assertion(validCase, result);
        });
}

/**
 * Return a shuffled copy of an array.
 * @param {Array} a An array containing items to be shuffled.
 * @returns {Array} Shuffled copy of the array
 */
function shuffle(a) {
    let b = [...a];

    for (let i = a.length - 1; 0 < i; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [b[i], b[j]] = [b[j], b[i]];
    }
    return b;
}

/**
 * @typedef {Object} Runner
 * @property run {Function} Runner.run
 *
 * @param {String} mode
 * @returns {Runner}
 */
const createRunner = (mode = 'async') => {return 'sync' === mode ? {run: syncRun} : {run: asyncRun}};

function isAsync(func) { return (func.toString().trim().match(/^async /)) }

const assertArrayEquality = function (testCase, validCase) {expect(testCase).toEqual(validCase)};


/**
 * Runs each member of ...fnTests on each member of the loaded dataset
 * @param runner {Runner} the test runner
 * @param datasetPath {String} path to data set file
 * @param testTarget {Function} the function to apply to members of the data set
 * @param assertions {...Function} assertions to test on each member of the data set
 */
function testSetInClosure(runner, datasetPath, testTarget, ...assertions) {
    let dataSetFile = fs.readFileSync(__dirname + "/../../" + datasetPath);
    let testCases = JSON.parse(dataSetFile);

    for (let validCase of testCases) {
        for (let assert of assertions) {
            runner.run(assert, validCase, shuffle(validCase), testTarget);
        }
    }
}

/**
 * @param {function(a: *)} strategy
 */
module.exports.sortingStrategyTest = function (strategy) {
    /**
     * Test name
     * @type {string}
     */
    let name = String(isAsync(strategy) ?
        "Async " : "Sync ").concat("sorting method ", strategy.name.toString());

    /**
     * Test runner
     * @type {Runner}
     */
    let runner = isAsync(strategy) ?
        createRunner('async') :
        createRunner('sync');

    describe(name,
        () => {
            jest.setTimeout(300000);
            const boundSortingStrategy = strategy;
            it('considers empty arrays sorted', () => {
                runner.run(
                    assertArrayEquality,
                    [],
                    [],
                    boundSortingStrategy
                );
            });

            it('considers single element arrays sorted', () => {
                runner.run(
                    assertArrayEquality,
                    [5],
                    [5],
                    boundSortingStrategy
                );
            });

            it('properly sorts integer arrays of size 1 - 10', () => {
                testSetInClosure(runner, 'datasets/smallIntSetOneK.JSON', boundSortingStrategy, assertArrayEquality);
            });

            it('properly sorts integer arrays of size 11 - 100', () => {
                testSetInClosure(runner, 'datasets/mediumIntSetOneHundred.JSON', boundSortingStrategy, assertArrayEquality);
            });

            it('properly sorts integer arrays of size 101 - 1000', () => {
                testSetInClosure(runner, 'datasets/largeIntSetTen.JSON', boundSortingStrategy, assertArrayEquality);
            });

            it('properly sorts integer arrays of size 1001 - 100000', () => {
                testSetInClosure(runner, 'datasets/absurdIntSetTen.JSON', boundSortingStrategy, assertArrayEquality);
            });
            // it('properly sorts integer arrays of 1 000 000', () => {
            //     testSetInClosure(runner, 'datasets/OneMillionIntegerSet.JSON', boundSortingStrategy, assertArrayEquality);
            // });
        });
};