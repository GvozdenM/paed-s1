const Comparator = require('../../src/Comparators/MixComparator');
const comparator = Comparator.createMixedComparator();

const testCases = {
    equalWinRatesDifferentKDA: {
        mockPlayerSmallerKda: {
            winrate: 50,
            getKda: function() {
                return 2;
            }()
        },
        mockPlayerLargerKda: {
            winrate: 50,
            getKda: function() {
                return 5;
            }()
        }
    },

    equalKdaDifferentWinRates: {
        mockPlayerWithHigherWinrate: {
            winrate: 51.11,
            getKda: function() {
                return 2;
            }()
        },

        mockPlayerWithLowerWinrate: {
            winrate: 35,
            getKda: function() {
                return 2;
            }()
        }
    },

    similarWinrateVariantKda: {
        mockPlayerWithLowKda: {
            winrate: 51,
            getKda: function() {
                return 2;
            }()
        },

        mockPlayerWithHighKda: {
            winrate: 50,
            getKda: function() {
                return 7.56;
            }()
        }
    }

};

describe('Mixed Player Comparator', () => {
    it('defaults to sorting by kda if player win rates are equal', () => {
        expect(comparator.compare(
            testCases.equalWinRatesDifferentKDA.mockPlayerLargerKda,
            testCases.equalWinRatesDifferentKDA.mockPlayerSmallerKda
            )).toBeGreaterThan(0);
    });

    it('gives priority to win rate when kda is equivalent', () => {
        expect(comparator.compare(
            testCases.equalKdaDifferentWinRates.mockPlayerWithHigherWinrate,
            testCases.equalKdaDifferentWinRates.mockPlayerWithLowerWinrate
        )).toBeGreaterThan(0);
    });

    it('considers a player with a bit lower winrate but much higher kda as better than' +
        'a player with lower kda but bit higher kda', () => {
        expect(comparator.compare(
            testCases.similarWinrateVariantKda.mockPlayerWithHighKda,
            testCases.similarWinrateVariantKda.mockPlayerWithLowKda
        )).toBeGreaterThan(0);
    });
});