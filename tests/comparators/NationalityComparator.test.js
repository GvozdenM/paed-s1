const NationalityComparator = require('../../src/Comparators/NationalityComparator');

const players = {
    player1: {
        name: "Player 1",
        nationality: "a"
    },

    player2: {
        name: "Player 2",
        nationality: "b",
    },

    player3: {
        name: "Player",
        nationality: "Norwegian"
    },

    player4: {
        name: "Player",
        nationality: "Norwegian"
    },

    player5: {
        name: "a",
        nationality: "c"
    },

    player6: {
        name: "b",
        nationality: "c"
    }
};

const comparator = function setUp() {
    return NationalityComparator.createComparator();
}();

describe('Nationality comparator ', () => {
    it('considers a player with nationality a as being smaller than one with nationality b', () => {
        expect(comparator.compare(players.player1, players.player2)).not.toBeGreaterThan(0);
    });

    it('considers players with same nationality and same name as equal', () => {
        expect(comparator.compare(players.player3, players.player4)).toBeCloseTo(0);
    });

    it('considers player with name a as bigger than player with name b of the same nationality', () => {
        expect(comparator.compare(players.player5, players.player6)).not.toBeGreaterThanOrEqual(0);
    });
});