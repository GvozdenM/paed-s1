const WinRateComparator = require('../../src/Comparators/WinrateComparator');

const teamObj1 = {
    winrate: 11.32
};

const teamObj2 =  {
    winrate: 11.00
};

const teamObj3 =  {
    winrate: 11.00
};

const comparator = function setUp() {
    return WinRateComparator.createWinRateComparator();
}();

it('returns > 0 if first object has higher winrate', () => {
    expect(comparator.compare(teamObj1, teamObj2)).toBeGreaterThan(0);
});

it('returns < 0 if second object has a higher winrate than first', () => {
    expect(comparator.compare(teamObj2, teamObj1)).not.toBeGreaterThanOrEqual(0);
});

it('returns 0 if objects are equal in winrate', () => {
    expect(comparator.compare(teamObj2, teamObj3)).toBeCloseTo(0, 10);
});