const es = require('event-stream');
const timedDataStream = require('../../src/utils/timedDataStream');
const MixedPlayerSorterFactory = require('../../src/factories/MixedPlayerSorterFatory');
const TeamWinRateSorterFactory = require('../../src/factories/TeamWinRateSorterFactory');
const NationalityPlayerSorterFactory = require('../../src/factories/NationalityPlayerSorterFactory');

let input = [];
let winRateSorted = [];
let nationalitySorted = [];
let mixedSorted = [];

beforeAll(() => {
    const capture = function(data) {
        input.push(data);
    };

    let timedIOStream = timedDataStream
        .createTimedDataStream(__dirname + '/../datasets/datasetXL.json')
        .pipe(es.mapSync((data) => {
            capture(data);
        }));

    winRateSorted = [...input];
    nationalitySorted = [...input];
    mixedSorted = [...input];


    winRateSorted.sort((a, b) => {
        return a.winrate - b.winrate;
    });

    nationalitySorted.sort((a, b) => {
        if(0 === a.nationality.localeCompare(b)) {
            return a.name.localeCompare(b);
        }
        else {
            return  a.nationality.localeCompare(b);
        }
    });
});

describe('sorts small data set', () => {


    let strategyNames = ['mergesort', 'quicksort', 'radixSort', 'bucketSort'];
    let aInput = [...input];

    for(let strategyName of strategyNames) {
        test('based on win rate', () => {
            let sorter = TeamWinRateSorterFactory.createSorter(strategyName);
            expect(sorter.sort(aInput)).toEqual(winRateSorted);
        });

        if('radixSort' != strategyName) {
            test('based on nationality', () => {
                let sorter = NationalityPlayerSorterFactory.createSorter(strategyName);
                expect(sorter.sort(aInput)).toEqual(nationalitySorted);
            });
        }
    }
});

