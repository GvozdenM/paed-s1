const Fs = require('fs');
const es = require('event-stream');
const MixedPlayerSorterFactory = require('../../src/factories/MixedPlayerSorterFatory');
const TeamWinRateSorterFactory = require('../../src/factories/TeamWinRateSorterFactory');
const NationalityPlayerSorterFactory = require('../../src/factories/NationalityPlayerSorterFactory');
const JSONStream = require('JSONStream');
const augmentedPlayer = require('../../src/PlayerObjectAugmenter');

let input = [];
let winRateSorted = [];
let nationalitySorted = [];
let mixedSorted = [];
let players = [];
let strategyNum = 0;
let strategyNames = ['radixSort', 'mergesort', 'quicksort', 'bucketSort'];
let strategy = strategyNames[strategyNum];

function shuffle(a) {
    let b = [...a];

    for (let i = a.length - 1; 0 < i; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [b[i], b[j]] = [b[j], b[i]];
    }
    return b;
}

jest.setTimeout(3000000);

beforeAll((done) => {
        Fs.createReadStream(__dirname + '/../../datasets/datasetS.json', {encoding: 'utf8'})
            .pipe(JSONStream.parse('*'))
            .pipe(es.mapSync((aTeam) => {
                let i = 0;
                for(let aPlayer of aTeam.jugadores) {
                    let bPlayer = augmentedPlayer.augmentTeamPlayerObjects(aTeam.winrate, aPlayer);
                    nationalitySorted.push(bPlayer);
                    mixedSorted.push(bPlayer);
                    players.push(bPlayer);
                    i++;
                }
                aTeam.jugadores = undefined;
                winRateSorted.push(aTeam);
                input.push(aTeam);
            }))
            .on('close', () => {
                setTimeout(() => {
                    winRateSorted.sort((a, b) => {
                        return a.winrate - b.winrate;
                    });

                    nationalitySorted.sort((a, b) => {

                    });

                    mixedSorted.sort((a, b) => {

                        const winRateWeight = 0.65;

                        const kdaWeight = 0.35;

                        const calculateWeight = function (a) {
                            return (a.winrate * winRateWeight) + (a.getKda * kdaWeight);
                        };

                        return calculateWeight(a) - calculateWeight(b);
                    });

                    done();
                }, 20000);
            });
});

describe('Testing small data set', () => {
    const testStrategy = function (strategy) {
        describe('Using strategy ' + strategy + ' set is sorted correctly by ', () => {
            beforeEach(() => {
                input = shuffle(input);
                players = shuffle(players);
            });

            test('win rate', () => {
                let sorter = TeamWinRateSorterFactory.createSorter(strategy);
                let output = sorter.sort(input);

                for(let i = 1; i < output.length - 1; i++) {
                    expect(output[i].winrate).toBeGreaterThanOrEqual(output[i - 1].winrate);
                }

                for(let i = 0; i < input.length - 1; i++) {
                    expect(output).toContain(input[i]);
                }
            });

            test('nationality alphabetically with a fallback to name alphabetically', () => {
                if('radixSort' !== strategy) {
                    let sorter = NationalityPlayerSorterFactory.createSorter(strategy);
                    let output = sorter.sort(players);

                    for(let i = 0; i < players.length - 1; i++) {
                        expect(output).toContain(players[i]);
                    }

                    for(let i = 0; i < output.length - 1; i++) {
                        if(output[i].nationality === output[i + 1].nationality && output[i].name === output[i + 1].name) {
                            continue;
                        }
                        else {
                            expect(output[i]).toEqual(nationalitySorted[i]);
                        }
                    }
                }
            });

            test('by a mix of weighted criteria win rate and kill-death-assist ratio', () => {
                if('radixSort' !== strategy) {
                    let sorter = MixedPlayerSorterFactory.createSorter(strategy);
                    expect(sorter.sort(players)).toEqual(mixedSorted);
                }
            });
        });
    };

    while(strategyNames[strategyNum]) {
        strategy = strategyNames[strategyNum];
        testStrategy(strategy);
        strategyNum++;
    }
});

