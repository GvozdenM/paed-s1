const es = require('event-stream');
const Sorter = require('./src/sorting/Sort');
const strat = require('./src/strategies/QuickSort');
const Comparator = require('./src/utils/Comparator');
const timedDataStream = require('./src/utils/timedDataStream');

let sorter = new Sorter({
    comparator: new Comparator((a , b) => {
        return Comparator.defaultComparisonFunction(b.winrate, a.winrate);
    }),
    visitor: () => {},
    sortingStrategy: strat.quicksort
});

let input = [];
const capture = function(data) {
    input.push(data);
};

let timedIOStream = timedDataStream
    .createTimedDataStream(__dirname + '/datasets/datasetXL.json')
    .pipe(es.mapSync((data) => {
        capture(data);
    }))
    .on('close', () => {
        console.log(sorter.sort(input));
    });



let array = [];
for(let i = 0; i < 1000000; i++) {
    array.push(i);
}

console.log(array);

const teams = JSON.parse(data_stream);

