Installing Node JS 

To install the NodeJS it is recommended to work with a Linux system. This documentation describes how to do so. For Windows alternatives guides are available online. 

 

To install Node JS and the node package manager (Npm), in a linux terminal window run the following commands: 

 

sudo apt-get install curl 

curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash - 

 

sudo apt get update 

 

sudo apt install nodejs 

 

sudo apt install npm 

 

More information can be found here. 

For a Windows download or .deb for non-apt Linux distros look here. A snap version of both npm and node is available. 

 

Provisioning Dependencies 

We will now use the node package manager installed in the previous step to provision the projects dependencies. This will create a node_modules folder inside the root of the project. 

 

Ensuring you are in the project root (where package.json is lockated) run: 

npm install package.json 

 

Running the project 

At this point the project is setup and ready to run. Primarily, the interest is running the test suit, however, console UI access is also provided. To gain console access run from the project root: 

 

node src/index.js 

 

To run the test suit simply type: 

 

jest 

 

Installing WebStorm 

It is highly recommended to view the project through an integrated development environment (IDE). We have chosen to recommend WebStorm for a number of reasons. Based on IntelliJ, WebStrom is a modern and versatile IDE with the most recent capabilities in terms of debugging. It also has support for a large range of frameworks (such as jest) and verson control integration.  

WebStorm can be installed using the following guide. 

Running jest tests can be done by following this guide. 
