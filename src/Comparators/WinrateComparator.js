const Comparator = require('../utils/Comparator');

module.exports.createWinRateComparator = function createWinRateComparator() {
    const keyGetter = (a) => {
        return a.winrate ? a.winrate : 0;
    };

    return new Comparator((a , b) => {
        return Comparator.defaultComparisonFunction(a, b, keyGetter);
    }, keyGetter);
};