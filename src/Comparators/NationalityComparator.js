const Comparator = require('../utils/Comparator');

module.exports.createComparator = function createComparator() {
    const keyGetter = (a) => {
        return a.nationality;
    };

    const nameKeyGetter = (a) => {
        return a.name;
    };

    return new Comparator((a, b) => {
        let comparisonResult = Comparator.defaultComparisonFunction(a, b, keyGetter);
        if (0 === comparisonResult) {
            return Comparator.defaultComparisonFunction(a, b, nameKeyGetter);
        } else {
            return comparisonResult;
        }
    });
};