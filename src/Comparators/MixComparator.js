const Comparator = require('../../src/utils/Comparator');

/**
 * The weight determining how important
 * the winrate of a player is to determining
 * their skill level compared to another player
 * @type {number}
 */
const winRateWeight = 0.65;

/**
 * The weight determining how important
 * the kill-death-assist ratio of a player is
 * to determiningm their skill level compared to another player
 * @type {number}
 */
const kdaWeight = 0.35;

/**
 * @param {AugmentedPlayer} a
 **/
const calculateWeight = function (a) {
    return (a.winrate * winRateWeight) + (a.getKda * kdaWeight);
};

/**
 * Mixed player comparison function compares two players
 * in terms of perceived skill dependant on two variables.
 * Their win rate and their kill-death-assist ratios with
 * the former being given more priority.
 * @param {AugmentedPlayer} a
 * @return {Number}
 */
const mixedPlayerComparisonFunction = function (a, b) {
    return Comparator.defaultComparisonFunction(calculateWeight(a), calculateWeight(b));
};

module.exports.createMixedComparator = function createMixedComparator() {
    let keyGetter = function (a) {
        return calculateWeight(a);
    };


    let comparator = new Comparator((a, b) => {
        return mixedPlayerComparisonFunction(a, b);
    }, keyGetter);

    return comparator;
};