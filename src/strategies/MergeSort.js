const Comparator = require('../../src/utils/Comparator');

/**
 * Merge Sort
 *
 * Time: O(N*logN)
 * Space: O(N)
 *
 */

let aux = [];

/**
 * Merge two sub-arrays [low, mid], [mid + 1, high] into sorted one
 */
/* TODO: change this code */



const merge = function(arr, low, mid, high, comparator) {
    let i = low, j = mid + 1;
    let intComparator = new Comparator();

    for(let k = low; k <= high; k++) {
        aux[k] = arr[k];
    }

    for(let k = low; k <= high; k++) {
        if (intComparator.greaterThan(i, mid)) {
            arr[k] = aux[j];
            j++;
        } else if (intComparator.greaterThan(j, high)) {
            arr[k] = aux[i];
            i++;
        } else if (comparator.smallerThan(aux[j], aux[i])) {
            arr[k] = aux[j];
            j++;
        } else {
            arr[k] = aux[i];
            i++;
        }
    }
};

const findMid = (high, low) => {
    return Math.floor((high - low) / 2) + low;
};

const isDone = (high, low) => { return high <= low;};

const _mergeSort = function(a, lo, hi, comparator) {

    if(isDone(hi, lo)) {
        return a;
    }

    let mid = findMid(hi, lo);
    _mergeSort(a, lo, mid, comparator);
    _mergeSort(a, mid + 1, hi, comparator);
    merge(a, lo, mid, hi, comparator);

    return a;
};

const mergeSort = function(a, externalComparator) {
    let aCopy = [...a];
    let localComparator = externalComparator || new Comparator();
    return _mergeSort(aCopy, 0, a.length - 1, localComparator);
};

module.exports.sort = mergeSort;
