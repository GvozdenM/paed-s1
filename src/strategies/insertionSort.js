const Comparator = require('../utils/Comparator');

/**
 * @param {*[]} originalArray - Array to be sorted after being copied
 * @param {Comparator} comparator - Comparator to use for record comparison defaults to Number comparator
 * @return {*[]}
 */
module.exports.sort = function insertionSort(originalArray, comparator) {
    let length = originalArray.length;
    let array = [...originalArray];
    let localComparator = comparator || new Comparator();

    for(let i = 1; i < length; i++) {
        let j = 0;
        let temp = array[i];
        for(j = i - 1; 0 <= j && 0 < localComparator.compare(array[j], temp); j--) {
            array[j+1] = array[j];
        }
        array[j+1] = temp;
    }

    return array;
};
