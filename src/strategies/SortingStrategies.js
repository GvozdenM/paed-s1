const stub = (a, b) => {};
const quickSort = require('./QuickSort');
const bubbleSort = require('./BubbleSort');
const radixSort = require('./RadixSort');
const mergeSort = require('./MergeSort');
const insertionSort = require('./insertionSort');
const bucketSort = require('./bucketSort');    // why?

module.exports = {
    quickSort: quickSort.quicksort,
    bubbleSort: bubbleSort.sort,
    mergeSort: mergeSort.sort,
    radixSort: radixSort.radixsort,
    bucketSort: bucketSort.bucketSort,
    insertionSort: insertionSort.sort
};