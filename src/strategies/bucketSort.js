const Comparator = require('../utils/Comparator');
const insertionSort = require('./insertionSort').sort;


const findMinimumAndMaximum = function findMinMax(array, localComparator, keyGetter) {
    let [minValue, maxValue] = [{},{}];

    for(let value of array) {
        if(localComparator.smallerThan(value, minValue)) {
            minValue = value;
        }
        else if(localComparator.greaterThan(value, maxValue)) {
            maxValue = value;
        }
    }
    return {
        minValue: minValue,
        maxValue: maxValue,
    };
};

const calcBucketCount = function calcBucketCount(maxVal, minVal, bucketSize) {
    return Math.floor((maxVal - minVal) / bucketSize) + 1;
};

const createBuckets = function createBuckets(numBuckets) {
    let buckets = Array(numBuckets);

    for(let i = 0; i < buckets.length; i++) {
        buckets[i] = [];
    }

    return buckets;
};


const calcBucketNum = function calcBucketNum(currentVal, minimum, bucketSize) {
    return Math.floor((currentVal - minimum) / bucketSize);
};

const pushToBuckets = function pushToBuckets(array, buckets, bucketSize, minimum, keyGetter) {
    array.forEach(function (currentVal) {
        buckets[calcBucketNum(keyGetter(currentVal), minimum, bucketSize)].push(currentVal);
    });

    return buckets;
};


const _bucketSort = function _bucketSort(array, bucketSize = 10, comparator, keyGetter) {
    //arrays of length 1 or less are already sorted
    if (1 >= array.length) {
        return array;
    }

    let minMax = findMinimumAndMaximum(array, comparator, keyGetter);
    let [min, max] = [keyGetter(minMax.minValue), keyGetter(minMax.maxValue)];

    // Initializing buckets
    let numBuckets = calcBucketCount(max, min, bucketSize);
    let buckets = createBuckets(numBuckets);

    // Pushing values to buckets
    buckets = pushToBuckets(array, buckets, bucketSize, min, keyGetter);

    // Sorting buckets
    let result = [];

    buckets.forEach(function(bucket) {
        insertionSort(bucket, comparator).
        forEach(function (element) {
            result.push(element)
        });
    });

    return result;
};

/**
 * Implementation of bucket sort depend on comparator comparator interface
 * @param {Array} originalArray
 * @param {Comparator} comparator
 * @param {Function} [keyer] - Returns integer value for the member passed
 */
module.exports.bucketSort = function (originalArray, comparator, keyer) {
    //default to default integer comparator if no comparator is provided
    let localComparator = new Comparator();
    let keyGetter = comparator.keyGetter || function(x) { return x };

    let array = [...originalArray];

    return _bucketSort(array, 10, comparator, keyGetter);
};
