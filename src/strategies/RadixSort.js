const Comparator = require('../utils/Comparator');

// The main function to that sorts arr[] of size n using
 // Radix Sort
 module.exports.radixsort = function radixsort(teamObjArr, comparator) {
     let array = [...teamObjArr];

     if(1 >= array.length) {
         return array;
     }

     let getProperty = comparator.compare || Comparator.defaultComparisonFunction;

     let antiDecimal = 1000;
     // Find the max number and multiply it by 10 to get a number
     // with no. of digits of max + 1
     const maxNum = Math.max.apply(Math, array.map(function(o) { return o.winrate * antiDecimal; })) * 10;

     let divisor = 10;
     while (divisor <= maxNum) {
         // Create bucket arrays for each of 0-9
         let buckets = [...Array(10)].map(() => []);
         // For each number, get the current significant digit and put it in the respective bucket
         for (let obj of array) {
             buckets[Math.floor((obj.winrate * antiDecimal % divisor) / (divisor / 10))].push(obj);
         }
         // Reconstruct the array by concatinating all sub arrays
         array = [].concat.apply([], buckets);
         // Move to the next significant digit
         divisor *= 10;
     }
     return array;
 };