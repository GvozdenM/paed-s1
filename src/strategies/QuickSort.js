const Comparator = require('../utils/Comparator');

module.exports.quicksort = function quicksort(originalArray, comparator) {
    //create shallow copy of original array to prevent from modification
    const array = [...originalArray];

    // If array has less than or equal to one elements then it is already sorted.
    if (1 >= array.length) {
        return array;
    }

    const leftArray = [];
    const rightArray = [];

    // Take the first element of array as a pivot.
    const pivotElement = array.shift();
    const centerArray = [pivotElement];

    // Split all array elements between left, center and right arrays.
    while (array.length) {
        const currentElement = array.shift();
        const comparison = comparator.compare(currentElement, pivotElement);

        if(0 === comparison){
            centerArray.push(currentElement);
        }
        else if(0 < comparison) {
            rightArray.push(currentElement);
        } else {
            leftArray.push(currentElement);
        }
    }

    // Sort left and right arrays.
    const leftArraySorted = quicksort(leftArray, comparator);
    const rightArraySorted = quicksort(rightArray, comparator);

    // Let's now join sorted left array with center array and with sorted right array.
    return leftArraySorted.concat(centerArray, rightArraySorted);
};