/**
 * @param {Array} arr
 * @param {Comparator} comparator
 * @returns {Array}
 */
module.exports.sort = function bubbleSort(arr, comparator){
    for (let i = arr.length; 0 <= i; i--){
        for(let j = 1; j <= i; j++){
            if(arr[j - 1] > arr[j]){
                let temp = arr[j - 1];
                arr[j - 1] = arr[j];
                arr[j] = temp;
            }
        }
    }
    return arr;
};