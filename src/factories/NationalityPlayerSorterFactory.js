const NationalityComparator = require('../Comparators/NationalityComparator');
const AbstractSorter = require('../sorting/Sort');
const sortingStrategies = require('../strategies/SortingStrategies');

module.exports.createSorter = function (strategyName) {
    let comparator = new NationalityComparator.createComparator();
    let strategy;

    switch (strategyName) {
        case 'mergesort':
            strategy = sortingStrategies.mergeSort;
            break;
        case 'quicksort':
            strategy = sortingStrategies.quickSort;
            break;
        case 'bucketSort':
            strategy = sortingStrategies.bucketSort;
            break;
        case 'radixSort':
            strategy = sortingStrategies.radixSort;
            break;
        default:
            throw new Error("Invalid Sorting strategy!")
    }

    return new AbstractSorter({
        comparator: comparator,
        sortingStrategy: strategy
    });
};