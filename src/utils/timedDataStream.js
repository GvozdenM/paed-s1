const Fs = require('fs');
const JSONStream = require('JSONStream');


const DEFAULT_LOG_PATH = __dirname + '/datasetsS.json';

module.exports = {
    createTimedDataStream:
        function createTimedDataStream(logPath = DEFAULT_LOG_PATH) {

            /**
             * Variable to capture process.hrtime() when I/O starts
             */
            let iostart;

            /**
             * Variable to capture result of process.hrtime(iostart) i.e. execution time of I/O
             */
            let ioend;

            /**
             * Internal string containing the path from which readable stream is created
             * @type {string}
             */
            let path = logPath;

            let parser = JSONStream.parse('*');

            return Fs.createReadStream(path, {encoding: 'utf8'})
                .on('ready', () => {
                    console.log("Starting I/O...");
                    iostart = process.hrtime();
                })
                .on('close', () => {
                    ioend = process.hrtime(iostart);
                    console.info('I/O time (hr): %ds %dms', ioend[0], ioend[1] / 1000000);
                })
                .pipe(parser);
        }
};