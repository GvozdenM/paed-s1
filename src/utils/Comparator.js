module.exports = class AbstractComparator {
    /**
     * @param {function(a: *, b: *)} [comparisonFunction] - Custom function to compare two values, defaults
     * to String | Number comparison function
     * @param {function(a:*)} [keyGetter]
     */
    constructor(comparisonFunction, keyGetter) {
        this.comparisonFunction = comparisonFunction || AbstractComparator.defaultComparisonFunction;
        this.keyGetter = keyGetter || AbstractComparator.defaultKeyGetter;
        this.compare = (a, b) => {return this.comparisonFunction(a, b, this.keyGetter)};
    }

    static defaultKeyGetter(a) {
        return a;
    }

    /**
     * Returns the differance between the values a and b
     * @param {(String | Number)} a
     * @param {(String | Number)} b
     * @param keyGetter
     * @returns {number}
     */
    static defaultComparisonFunction(a, b, keyGetter) {

        /**
         *
         * They are equal so bypass more complex comparisons
         */
        if(a === b) {
            return 0;
        }

        /**
         * lexographically compare if a and b are strings
         */
        if("string" === typeof a || "string" === typeof b) {
            return a.localeCompare(b);
        }

        if("number" === typeof a || "number" === typeof b) {
            return a - b;
        }

        /**
         * a and b are numbers so return difference
         */
        return keyGetter(a) - keyGetter(b);
    }

    equals(a, b) {
        return 0 === this.compare(a, b);
    }

    greaterThan(a, b) {
        return 0 < this.compare(a, b);
    }

    smallerThan(a, b) {
        return 0 > this.compare(a, b);
    }
};