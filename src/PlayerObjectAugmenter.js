/**
 * @typedef Legend
 * @property {String} name
 */

/**
 * @typedef Player
 * @property {String} name
 * @property {Number} total_kills
 * @property {Number} total_deaths
 * @property {Number} total_assistances
 * @property {String} nationality
 * @property {String} main_position
 * @property {[{name: string}]} main_champions
 */

/**
 * @typedef AugmentedPlayer
 * @typedef Player
 * @property {String} name
 * @property {Number} total_kills
 * @property {Number} total_deaths
 * @property {Number} total_assistances
 * @property {String} nationality
 * @property {String} main_position
 * @property {Number} winrate
 * @property {[{name: string}]} main_champions
 * @property {Number} getKda - returns the players kill death assist ration
 */

/**
 * @param {Number} teamWinRate - The winrate of the team to which the players array belong
 * @param {Player} player to be augmented
 * @return {AugmentedPlayer}
 */
module.exports.augmentTeamPlayerObjects = function augmentPlayerObjects(teamWinRate, player) {
    return Object.create(player, {
        getKda: {
            configurable: true,
            writable: false,
            value: function getKda() {
                const killContribution = (player.total_kills + (player.total_assistances / 2));

                if(player.total_deaths) {
                    return killContribution / player.total_deaths;
                }
                else {
                    return killContribution;
                }
            }(),
        },
        winrate: {
            writable: true,
            configurable: true,
            value: teamWinRate
        }
    });
};