/**
 * @typedef {Object} SortingCallbacks
 * @property {Object} comparator - If provided this function is used to do all
 * the comparisons
 * @property {function(a: *, b: Object)} sortingStrategy - Required function that accepts two parameters,
 * an array and a comparison function to be used in sorting.
 * @property [function(a: *)] visitors - If provided this function will be called with each
 * element of the collection as a parameter being sorted
 */
module.exports = class AbstractSorter {
    /**
     * @param {SortingCallbacks} sortingConfiguration
     */
    constructor(sortingConfiguration) {
        this.callbacks = AbstractSorter.initSortingConfiguration(sortingConfiguration);
    }

    /**
     * @param {SortingCallbacks} sortingConfiguration
     * @returns {SortingCallbacks}
     */
    static initSortingConfiguration(sortingConfiguration) {
        const callbacks = sortingConfiguration || {};
        const stub = () => {};

        callbacks.comparator = callbacks.comparator || {};
        callbacks.visitor = callbacks.visitor || stub;
        if(!callbacks.sortingStrategy) {
            throw new Error("Hi");
        }

        return callbacks;
    }

    sort(array) {
        return this.callbacks.sortingStrategy(array, this.callbacks.comparator, this.callbacks.comparator.keyGetter);
    }
};